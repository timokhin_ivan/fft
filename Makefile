OPTC=-Wall -Werror -O0 -std=c99
OPTL=-lmkl_cdft_core -lmkl_intel_lp64 -lmkl_core -lmkl_sequential -lmkl_blacs_intelmpi_lp64 -lpthread -lm -ldl
CC=mpiicc

.PHONY: deploy

deploy: fft
	cp $< ${HOME}/_scratch/$<

fft: fft.c
	${CC} $< ${OPTC} ${OPTL} -o $@