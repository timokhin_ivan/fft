#include <stdlib.h>
#include <stdio.h>

#include <mpi.h>
#include <mkl_types.h>
#include <mkl_cdft.h>

#define CHECK(call, exp, type) (check((call), exp, #call, type, __FILE__, __LINE__))

#define MPI_CHECK(call) CHECK((call), MPI_SUCCESS, "MPI")

#define DFTI_CHECK(call) CHECK((call), DFTI_NO_ERROR, "DFTI")

void check(
  int code,
  int expected,
  const char* call,
  const char* type,
  const char* file,
  int line);

void finalize(void);

double time_fft(MKL_LONG n);

// Globals.  So sue me.
int mpi_rank, mpi_size;

#define ON_MASTER if (mpi_rank == 0)

const int SIZES[] = {
  1 << 23, 
  1 << 24, 
  1 << 25, 
  1 << 26,
  1 << 27,
  1 << 28,
  1 << 29
};

const int NTESTS = sizeof(SIZES) / sizeof(SIZES[0]);

int main(int argc, char** argv)
{
  MPI_CHECK(MPI_Init(&argc, &argv));
  atexit(finalize);

  MPI_CHECK(MPI_Comm_size(MPI_COMM_WORLD, &mpi_size));
  MPI_CHECK(MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank));

  ON_MASTER printf("Running on %d nodes\n", mpi_size);

  for (int i = 0; i < NTESTS; ++i)
  {
    double time = time_fft(SIZES[i]);

    ON_MASTER printf("%d\t%f\n", SIZES[i], time);
    ON_MASTER fflush(stdout);
  }

  return 0;
}

double time_fft(MKL_LONG n)
{
  double start_time = 0.0, end_time = 0.0;
  MKL_LONG local_len = 0, i = 0;
  DFTI_DESCRIPTOR_DM_HANDLE desc;
  MKL_Complex16* x;

  DFTI_CHECK(DftiCreateDescriptorDM(MPI_COMM_WORLD, &desc, DFTI_DOUBLE, DFTI_COMPLEX, 1, n));

  DFTI_CHECK(DftiGetValueDM(desc, CDFT_LOCAL_SIZE, &local_len));
  x = (MKL_Complex16*) calloc(local_len, sizeof(MKL_Complex16));

  DFTI_CHECK(DftiCommitDescriptorDM(desc));

  start_time = MPI_Wtime();
  DFTI_CHECK(DftiComputeForwardDM(desc, x));
  end_time = MPI_Wtime();

  DFTI_CHECK(DftiFreeDescriptorDM(&desc));
  free(x);

  return end_time - start_time;
}

void check(
  int code,
  int expected,
  const char* call,
  const char* type,
  const char* file,
  int line)
{
  if (code != expected)
  {
    fprintf(stderr, "Check failed (%s): %s (%s:%d) returned %d, expected %d\n",
	    type, call, file, line, code, expected);
    MPI_Abort(MPI_COMM_WORLD, 1);
  }
}

void finalize(void)
{
  MPI_Finalize();
}
